let changeState = (function () {
    let state = 0,
        lamps = ["Red", "Yellow", "Green"],
        lampsLength = lamps.length,
        order = [
            [4000, "Red"],
            [2000, "Red", "Yellow"],
            [4000, "Green"],
            [2000, "Yellow"]
        ],
        orderLength = order.length,
        lampIndex,
        orderIndex,
        sId;

    return function (stop) {
        if (stop) {
            clearTimeout(sId);
            return;
        }

        let lamp,
        lampDOM;

        for (lampIndex = 0; lampIndex < lampsLength; lampIndex += 1) {
            lamp = lamps[lampIndex];
            lampDOM = document.getElementById(lamp);
            if (order[state].indexOf(lamp) !== -1) {
                lampDOM.classList.add("lamp" + lamp);
            } else {
                lampDOM.classList.remove("lamp" + lamp);
            }
        }

        sId = setTimeout(changeState, order[state][0]);
        state += 1;
        if (state >= orderLength) {
            state = 0;
        }
    };
}());

document.querySelector("#trafficLight").addEventListener("click", (function () {
    let state = false;
    
    return function () {
        changeState(state);
        state = !state;
    };
}()), false);